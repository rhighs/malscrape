## Scraper

Requirements:<br>
    - Node.js ^14.15 <br>
    - NPM ^6.14 <br>
    - Redis ^6.0 <br>

### Usage

Clone this repository and install the required packages

```bash
$ git clone http://github.com/rhighs/malscrape
$ cd malscrape
$ npm install
```

Once finished you may proceed running the script. To do so just have a look at the help command

```bash
$ npm start -- --help
> scraper@1.0.0 start /home/rob/repos/malscrape
> node src/main.js "--help"

Usage:  npm start -- [command|value]

  npm start -- <minutes>:
        schedule this script to run at every <minutes> minutes.
  minutes <value>:
        integer number in [0..60].

  npm start -- --single-run | -s:
        start this script normally, no scheduling
```

Author: Roberto Montalti
