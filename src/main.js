import { createClient } from "redis";
import { writeFile } from "fs";
import { promisify } from "util";

import htmlToJson from "html-to-json";
import fetch from "node-fetch";
import cron from "node-cron";
import fs from "fs";

import { config } from "./config.js";
import { createDefaultPassGen } from "./passgen.js";

const URL = config.URL;
const DIR = config.DIR;
const REDIS_THREATS_KEY = config.REDIS_THREATS_KEY;
const genpass = createDefaultPassGen(config.PASS_LEN);

const initRedisClient = () => {
    const client = createClient();
    client.on("error", err => console.log("redis client error: ", err));
    return client;
}

const setupEnv = (dlDir) => {
    if (!fs.existsSync(dlDir))
        fs.mkdirSync(dlDir);
}

const createProgressDisplay = (target) => {
    let done = 0;
    return () => process.stdout.write(`\rProgress: ${++done}/${target}` + (done === target ? "\n" : ""));
}

const scrapeData_Heavy = (url) =>
    fetch(url)
        .then(res => res.text())
        .then(text => htmlToJson.parse(text, {
            rows: htmlToJson.createParser(["tr", {
                "namenHash": tr => {
                    // Remove useless tokens
                    let almostDone = tr.find("td").text()
                        .replace("   ", " ")
                        .replace("Download", "")
                        .replace("sample", "");

                    // Dedup tokens
                    almostDone = Array.from(new Set(almostDone.split(" "))).join(" ");

                    let lastSpaceIdx = almostDone.lastIndexOf(" ");
                    return [
                        almostDone.slice(0, lastSpaceIdx - 1).replace("\\\\", "\\").replace("\/", "-"),
                        almostDone.slice(lastSpaceIdx + 1, almostDone.length)
                    ];
                },
                "link": tr => {
                    return tr.find("a").attr("href");
                },
            }])
        }))
        .then(parsedDoc => parsedDoc.rows.filter)
        .then(data => data.filter(item => item.link)).finally()

const downloadInto = async (dir, filename, url) => {
    if (!url) {
        let error = "Got undefined url, skipping...";
        return [null, error];
    }

    let path = dir + filename;

    try {
        let res = await fetch(url)
            .then(x => x.arrayBuffer())
            .then(x => promisify(writeFile)(path, Buffer.from(x)));
        return [res, null];
    } catch (err) {
        let error = `Couldn't download resource: ${filename}\n\terr: ${err.toString()}`;
        return [null, error];
    }
}

const promisifiedDownload = (items, dlCallback) => {
    let proms = items.map(item => {
        let [threat, sha256] = item.namenHash;
        let filename = threat + "-" + sha256 + ".zip";

        return downloadInto(DIR, filename, item.link)
            .then(([_, err]) => {
                dlCallback();

                return {
                    threat: threat,
                    sha256: sha256,
                    localPath: !err ? DIR + filename : null,
                    password: genpass(),
                    downloadError: err
                }
            });
    });

    return Promise.all(proms);
}

const sync = async (redisClient) => {
    let jsonString = await redisClient.get(REDIS_THREATS_KEY);
    let oldThreats = jsonString !== "" ? JSON.parse(jsonString) : null;
    return !oldThreats ? firstRun(redisClient) : syncDiff(redisClient, oldThreats);
}

const firstRun = async (redisClient) => {
    let items = await scrapeData_Heavy(URL);
    console.log(`Initializing with ${items.length} items...`);

    return promisifiedDownload(items, createProgressDisplay(items.length))
        .then(objects => redisClient.set(REDIS_THREATS_KEY, JSON.stringify(objects)))
        .then(_ => items.length);
}

const syncDiff = async (redisClient, oldThreats) => {
    // Creates a hashmap [sha256, true] to better find missing threats
    let currentBySha = oldThreats
        .map(threat => [threat.sha256, true])
        .reduce((map, [key, value]) => { map.set(key, value); return map; }, new Map());

    // Finds untracked threats using what stated above
    let items = await scrapeData_Heavy(URL);
    let newThreats = items.filter(item => {
            let [_, sha256] = item.namenHash;
            return currentBySha.get(sha256) === undefined;
        });

    if (newThreats.length === 0) {
        return 0;
    }

    console.log(`Syncing ${newThreats.length} items...`);
    return promisifiedDownload(newThreats, createProgressDisplay(newThreats.length))
        .then(diffThreats => redisClient.set(REDIS_THREATS_KEY, JSON.stringify([...oldThreats, ...diffThreats])))
        .then(_ => newThreats.length)
}

const schedule = async (minutes) => {
    const redisClient = initRedisClient();
    await redisClient.connect();

    let cronstring = `*/${minutes} * * * *`;

    // Semaphore prevening jobs to overlap for small intervals
    let someRunning = false;
    cron.schedule(cronstring, async () => {
        if (!someRunning) {
            someRunning = true;
            let threatsAnalyzed = await sync(redisClient);
            console.log("Threats analyzed:", threatsAnalyzed);
            await redisClient.hSet(config.REDIS_RUNS_HASH_KEY, new Date().toISOString(), threatsAnalyzed);
            someRunning = false;
        }
    });
}

const singleRun = async () => {
    const redisClient = initRedisClient();
    await redisClient.connect();

    let threatsAnalyzed = await sync(redisClient);
    console.log("Threats analyzed:", threatsAnalyzed);
    await redisClient.hSet(config.REDIS_RUNS_HASH_KEY, new Date().toISOString(), threatsAnalyzed);
}

let help = "Usage:\tnpm start -- [command|value] \
\n\n  npm start -- <minutes>:\n\tschedule this script to run at every <minutes> minutes.\
\n  minutes <value>:\n\tinteger number in [0..60].\n \
\n  npm start -- --single-run | -s:\n\tstart this script normally, no scheduling\n";

(async () => {
    setupEnv(DIR);

    const args = process.argv.slice(2);
    let minutes = 0;

    if (args.length === 0) {
        console.error("You must provide some arguments, refer to --help for more info.");
        return;
    } else if (args.length >= 1) {
        if (args[0] === "--help" || args[0] === "-h") {
            console.log(help);
            return;
        }

        if (args[0] === "--single-run" || args[0] === "-s") {
            console.log("Starting single run...");
            await singleRun();
            process.exit(0);
        }

        minutes = parseInt(args[0]);
        if (isNaN(minutes)) {
            console.error("You must provide a valid number time interval.");
            return;
        }

        if (minutes <= 0 || minutes >= 60) {
            console.error("You must provide a number contained in [0..60].");
            return;
        }
    }

    await schedule(minutes);
})();
