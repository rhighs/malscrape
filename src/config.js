export const config = {
    URL: "https://das-malwerk.herokuapp.com/",
    DIR: "../samples/",
    PASS_LEN: 10,
    REDIS_THREATS_KEY: "osint:malware:dasmalwerk",
    REDIS_RUNS_HASH_KEY: "run:dasmalwerk",
}
