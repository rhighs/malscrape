import crypto from "crypto";

let randGenData = {
    index: 0,
    bytes: [],
    poolSize: 512
};

// Generates a word by [' ', ~) from https://www.asciitable.com/
const genRandPass = (length) => {
    const genRandValue = () => {
        if (randGenData.index >= randGenData.bytes.length) {
            randGenData.index = 0;
            randGenData.bytes = crypto.randomBytes(randGenData.poolSize);
        }

        return randGenData.bytes[randGenData.index++];
    }

    let pass = "";
    for (let _ = 0; _ < length; ++_) {
        let randvalue = genRandValue() % 94;
        pass += String.fromCharCode(33 + randvalue);
    }
    return pass;
}

const createDefaultPassGen = (length) => () => genRandPass(length);

export {
    genRandPass,
    createDefaultPassGen
}
